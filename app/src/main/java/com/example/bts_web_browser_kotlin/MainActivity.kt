package com.example.bts_web_browser_kotlin

import android.app.Activity
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.EditText
import android.widget.ImageButton
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    lateinit var webview:WebView
    lateinit var searchButton:ImageButton
    lateinit var backButton:ImageButton
    lateinit var urlText:EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        webview = findViewById(R.id.webview)
        urlText = findViewById(R.id.url)

        backButton = findViewById(R.id.ic_back_button)
        searchButton = findViewById(R.id.ic_search_button)

        webview.loadUrl("https://www.google.com")
        webview.settings.javaScriptEnabled = true // we need to enable javascript
        webview.canGoBack()
        webview.webViewClient = WebClient(this)

        backButton.setOnClickListener{
            webview.goBack()
        }

        searchButton.setOnClickListener{
            var searchText = urlText.text.toString().trim()
            webview.loadUrl(searchText)
        }



    }


    class WebClient internal constructor(private val activity: Activity): WebViewClient(){
        override fun shouldOverrideUrlLoading(
            view: WebView?,
            request: WebResourceRequest?
        ): Boolean {
            view?.loadUrl(request?.url.toString())
            return true
        }

    }
}